module recipe.domain {
    requires jackson.annotations;
    exports com.epm.recipe.domain.recipe;
    exports com.epm.recipe.domain.user;
    exports com.epm.recipe.domain;
}